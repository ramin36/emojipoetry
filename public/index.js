			var tr,td,img;
			var main = document.getElementById("main");			
			var emoCol = document.getElementById("emoCol");
			var poem = document.getElementById("poem");
			var poems = document.getElementById("poems");
			var poemsList = document.getElementById("poemList");
			var poemShort = [];
			var NEWLINE_ID = 0;
			var SPACE_ID = 1;

			var imgRootSource = "http://diskordier.net/public/emojis/";
			// local: no images here...
			//var imgRootSource = "imgs/";


// adding the emojis to the the emoCol div
			for(var i = 0; i < emojis.length; i++) {
			var table = document.createElement('table');
			var emoList = emojis[i];
			for(var e = 0; e < emoList.length;e++) {
				if(e % 20 == 0) 
					tr = table.insertRow(table.rows.length);

				td = tr.insertCell(tr.cells.length);  
				img = document.createElement("img");
				var imgSource = imgRootSource+emoList[e]; 
				//var	imgSource = "http://diskordier.net/public/emojis/"+emoList[e]; 
		    	img.src=imgSource
    			img.id=emoList[e];
    			img.setAttribute('width', '70%');
				img.setAttribute('height', '70%');
				img.onclick = function(e) {
					// relatedTarget , currentTarget, target, toElement
					var imageName = e.target.id;
					var inI = document.createElement("img");
			    	inI.src=imgRootSource+imageName;
	    			inI.setAttribute('width', '25px');
					inI.setAttribute('height', '25px');
					poem.appendChild(inI);
					poemShort.push(emoToID[imageName]);
				}; 
    			td.appendChild(img);
			}
			emoCol.appendChild(table);
		}

// 
//   POEM CREATION buttons
//
function newline() {
	poem.appendChild(document.createElement("br"));
	poemShort.push(NEWLINE_ID);
}

function space() {
	var inI = document.createElement("img");
	inI.src="imgs/space.png";
	inI.setAttribute('width', '25px');
	inI.setAttribute('height', '25px');
	poem.appendChild(inI);
	poemShort.push(SPACE_ID);
}

function del() {
	poem.removeChild(poem.childNodes[poem.childNodes.length-1]);
	poemShort = poemShort.splice(poemShort.length,1);
}

function submit() {
//	console.log(poemShort);
	var author = document.getElementById("author").value;
	var title = document.getElementById("title").value;

	if(author == "")
		author = "unnamed";
	if(title == "")
		title = "untitled";
	//console.log(poem);
	// converting to Hex doesnt save chars
	var poemString = "";
	for(var i =0; i < poemShort.length;i++) 
		//console.log(poemShort[i]);
		if(i < poemShort.length - 1)
			poemString += poemShort[i]+"+";
		else
			poemString += poemShort[i];
	clearPoem();
$.ajax({
        url: '/quote',
        type: 'GET',
        data: { author: author, title: title, text : poemString},
        contentType: 'application/text; charset=utf-8',
        success: function (response) {
        		console.log("posted");
        		showPoems();
        },
        error: function () {
             console.log("damn: error posting");
        }
    }); 
}

function clearPoem() {
	poemShort = new Array();
	while (poem.firstChild) 
	    poem.removeChild(poem.firstChild);
	document.getElementById("author").value = "";
	document.getElementById("title").value = "";
}

// button to switch from poem view to write view
function writePoem() {
	poems.style.display = 'none';
	main.style.display = 'block';	
}

// number of pages of poems loaded from the server
var pageCounter = 0;
var poemsOnServer = 0;

// button to switch from write view to poem view
function showPoems() {
	main.style.display = 'none';
	if(getNumberOfPoems()) {
		loadMorePoems();
	}
	//console.log("showPoems.pageCounter= "+pageCounter);
	else if(pageCounter == 0) 
		loadMorePoems();
	poems.style.display = 'block';
}

function loadMorePoems() {
	requestPoems(pageCounter);
}

function requestPoems(file) {
		$.ajax({
	 url: '/',
	 type: "POST",
	 data: {request:file},
	 success: function(response){
	 // alert('evaluate response and show alert');
	 if(response.poems !== undefined) {
		for(var i = response.poems.length -1; i >= 0;i--) {
			var div = appendPoem(response.poems[i]);
			if(i == response.poems.length -1) {
				var rect = div.getBoundingClientRect();
				window.scrollBy(0,rect.top-20);
			}
		}
	 	pageCounter++;
	 }
	 },
	 error: function(err){	 }
	}); 
}

function getNumberOfPoems() {
	var n = 'number';
		$.ajax({
	 url: '/',
	 type: "POST",
	 data: {request:n},
	 async:   false,
	 success: function(response){
	 // alert('evaluate response and show alert');
	 if(poemsOnServer != response.number) {
	 	pageCounter = 0;
	 	poemsOnServer = response.number;
	 	while (poemsList.firstChild) 
	   		poemsList.removeChild(poemsList.firstChild);
	   	return true;
	  // 	showPoems();
	 } else
	 	return false;
	 },
	 error: function(err){	 }
	});
}

function appendPoem(poem) {
	console.log("appending: "+poem);
	var div = $('<div/>')[0];
	var pjs = $.parseJSON(poem);
	div.appendChild(document.createTextNode(pjs.author));
	div.appendChild(document.createTextNode(" - "));
	div.appendChild(document.createTextNode(pjs.title));	
	div.appendChild(document.createElement("br"));
	for(var p = 0; p < pjs.text.length; p++) {
		//console.log(pjs.text[p] + " > "+emoAr[pjs.text[p]]);
		if(pjs.text[p] == NEWLINE_ID) 
			div.appendChild(document.createElement("br"));
		else {
			var inI = document.createElement("img");
			inI.src=imgRootSource+emoAr[pjs.text[p]];
			inI.setAttribute('width', '25px');
			inI.setAttribute('height', '25px');
			div.appendChild(inI);
		}
	}
	poemsList.appendChild(div);
	return div;
}


