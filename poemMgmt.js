var dbLogIt = true;

var counterFile = 'poems/count.txt';
var counter = 0;
var numberOfFiles = 0;

var fileToWriteIndex;
var fileToWrite; 
var POEMS_PER_FILE = 8;

var fs = require('fs');

module.exports = {
	init: function() { // get the counter from file into this memory
		var c = fs.readFileSync(counterFile);
		counter = parseInt(c);
		updateNumberOfFiles();
		console.log(counter +" poems stored in "+numberOfFiles +" files");
		updateFileToWrite();
		//resetAll();
	},
	addPoem: function(poem) { // adding a new Poem after a submit
		var poemS = (counter % POEMS_PER_FILE == 0 ? "" : "\n") + JSON.stringify(poem);
		fs.appendFile(fileToWrite, poemS, function(err) {
			if (err) throw err;
			countUp();
		});
	},
	getPoemsFromFile: function(file) {
		//console.log(file);
		file = numberOfFiles - file - 1;
		//console.log("requesting page: "+file);
		//console.log("numOfFiles: "+numberOfFiles);
		if(file <= numberOfFiles  && file >= 0) {
			var text = fs.readFileSync("poems/poem" + file + ".txt",'utf8');
			var sp = text.split("\n");
			console.log("serving page: "+ file);
			return sp; 
		} else {
			return null;
		}
	},
	getCounter: function() {
		return counter;
	}
};

function countUp() {
	counter++;
	var c = "" + counter;
	fs.writeFileSync(counterFile, counter);
	updateFileToWrite();
	updateNumberOfFiles(); 
	if (dbLogIt)
		console.log(counter + " saved poems now. in "+numberOfFiles +" files");
}

function updateFileToWrite() {
	fileToWriteIndex =	Math.floor(counter / POEMS_PER_FILE);
	fileToWrite = "poems/poem" + fileToWriteIndex + ".txt";
	if(counter % POEMS_PER_FILE == 0 )
		fs.writeFileSync(fileToWrite, "");
} 
 
function updateNumberOfFiles() {
 	numberOfFiles = Math.floor(counter / POEMS_PER_FILE)
 	+ (counter % POEMS_PER_FILE == 0 ? 0 : 1);
}
	


function resetAll() {
	counter = 0;
	fs.writeFileSync(counterFile, counter);
	for(var i=0; i <= fileToWriteIndex;i++) 
		fs.unlinkSync("poems/poem" + i + ".txt")
	updateFileToWrite();
}