var express = require('express');
var app = express();

app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded());
app.use(express.json());
//var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
/*
app.use(bodyParser());
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use( bodyParser.urlencoded() ); // to support URL-encoded bodies
*/

var mgmt = require('./poemMgmt');  

app.get('/', function(req, res) {
	res.status(404).sendfile('public/index.html');
});

app.post('/', function(req, res) {
	var requestS = req.param('request');
	console.log("incoming pageReq: "+requestS);
	if(requestS == 'number') {
		res.json({number:mgmt.getCounter()});
	}
	else { // a number for the page
		var ps = mgmt.getPoemsFromFile(requestS);
		if(ps != null) {
			var poems ={poems : ps};
		console.log(poems);
			res.json(poems);
		}
		else
			res.json({nomore:'sorry'});
	}
});

// was supposed to be a post. but JQuery post doesnt put the data into url

app.get('/quote', function(req, res) {
	//	console.log(req.query.author);
	//	console.log(req.param('author'));
	console.log(req.query.author + "- "+req.query.title+" : "+req.query.text);
/*	var Qauthor = req.query.author;
	var Qtext = req.query.text;
	if (!Qauthor) {
		res.statusCode = 400;
		return res.send('Error 400: Post syntax incorrect. author missing');
	}
	if (!Qtext) {
		res.statusCode = 400;
		return res.send('Error 400: Post syntax incorrect. text missing');
	}
*/
	var newPoem = {
		author: req.query.author,
		title: req.query.title,
		text: req.query.text.split("+")
	};
	mgmt.addPoem(newPoem);
//	quotes.push(newQuote);
	res.json(true);
});

function printTime() {
	var date = new Date();

    var hour = date.getHours();
    var min  = date.getMinutes();
    var sec  = date.getSeconds();
    return(hour+":"+min+":"+sec);
}

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
app.listen(server_port,server_ip_address);
console.log('Magic happens on port ' + server_port + " time:  "+printTime());
mgmt.init();